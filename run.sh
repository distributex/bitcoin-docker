#!/bin/sh
IMAGE=distributex/bitcoin
docker build -t $IMAGE .
exec docker run -ti --rm -v $(pwd):/workspace -w /workspace $IMAGE tmux
