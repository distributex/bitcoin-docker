FROM ubuntu
RUN apt update -yq && apt install -qy vim tmux git build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils libboost-all-dev
RUN apt-get install -yq software-properties-common && add-apt-repository ppa:bitcoin/bitcoin && apt-get update -yq && apt-get install -yq libdb4.8-dev libdb4.8++-dev
